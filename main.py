import os
import sys
from time import sleep
from PIL import Image

# ------------------------CONFIG-----------------------

FPS = 30  # To make the movement of the gifs less blurry, install dconf-editor and use it to disable background fading.

# TODO Allow automatic switching between gifs after time.

# TODO Pick gifs automatically from folder instead of relying on user input.

# ----------------------END CONFIG---------------------

if __name__ == "__main__":
    # Prepare...
    gif_name = sys.argv[1]
    imageObject = Image.open(f"./gifs/{gif_name}.gif")
    frames = imageObject.n_frames

    # Generate images...
    # TODO Only generate the new images if the folder for that gif doesnt already exist.
    os.makedirs(f"images/{gif_name}/", exist_ok=True)

    for frame in range(frames):
        imageObject.seek(frame)
        imageObject.convert("RGB").save(f"./images/{gif_name}/{frame}.jpg", "JPEG")

    # Set backgrounds...
    frame_time = round(1 / FPS, 2)  # time between frames in seconds
    cwd = os.path.abspath(os.getcwd())

    while True:
        for frame in range(frames):
            os.system(
                f"gsettings set org.gnome.desktop.background picture-uri file://{cwd}/images/{gif_name}/{frame}.jpg")
            sleep(frame_time)
